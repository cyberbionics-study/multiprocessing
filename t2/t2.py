import threading
import time
import os


def check_file(filename, event):
    while not os.path.exists(filename):
        print(f"file {filename} not found. wait 5 seconds.")
        time.sleep(5)

    while True:
        with open(filename, 'r') as file:
            content = file.read()

        if "Wow!" not in content:
            print(f'file {filename} exists, but there is no "Wow!" . wait 5 seconds.')
            time.sleep(5)
        else:
            print(f"string 'Wow!' found in file {filename}. generating event.")
            event.set()
            break


def delete_file_on_event(filename, event):
    event.wait()
    print(f"event happened. deleting file {filename}.")
    os.remove(filename)


filename = 'test_file.txt'

event = threading.Event()

check_thread = threading.Thread(target=check_file, args=(filename, event))
check_thread.start()

delete_thread = threading.Thread(target=delete_file_on_event, args=(filename, event))
delete_thread.start()

check_thread.join()
delete_thread.join()

print("program is over.")
