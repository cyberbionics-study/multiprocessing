import threading
from concurrent.futures import ThreadPoolExecutor
import time
from functools import wraps


def timing_decorator(func):
    """timer decorator"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"\nTime taken: {end_time - start_time:.5f} seconds")
        return result
    return wrapper


def factorial(n):
    """factorial function"""
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


@timing_decorator
def calculate_factorial_range(start, end):
    """calculating factorials in range"""
    for i in range(start, end + 1):
        factorial_result = factorial(i)
        print(f"\nFactorial of {i}: {factorial_result}")


@timing_decorator
def test_with_thread():
    num_tasks = 5
    task_range = (5, 15)

    threads = []
    for _ in range(num_tasks):
        thread = threading.Thread(target=calculate_factorial_range, args=task_range)
        threads.append(thread)

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


@timing_decorator
def test_with_thread_pool_executor():
    num_tasks = 5
    task_range = (5, 15)

    with ThreadPoolExecutor(max_workers=num_tasks) as executor:
        futures = [executor.submit(calculate_factorial_range, *task_range) for _ in range(num_tasks)]
        for future in futures:
            future.result()


if __name__ == "__main__":
    test_with_thread()

    # test_with_thread_pool_executor()
